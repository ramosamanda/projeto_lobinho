import data from "./lobinhos.json" assert { type: "json" };

if(document.querySelector('#index_lobos')){
    addImagenseDescricaoLobinhos(document.querySelector('#index_lobos'))
}
function criarElementosHTMLIndex(data){
    return`
   <h2 class="lobos_titulo">Lobos Exemplo</h2>
   <div class="lobo_container_esq">
       <img src= ${data[0].imagem} alt="" class="lobo_esq">
       <div class="lobo_esq_info">
           <h2 class="lobo_esq_nome">${data[0].nome}</h2>
           <h3 class="lobo_esq_idade">Idade: ${data[0].idade} anos</h3>
           <p class="lobo_esq_desc">${data[0].descricao}</p>
       </div>
   </div>

   <div class="lobo_container_dir">
       <div class="lobo_dir_info">
           <h2 class="lobo_dir_nome">${data[7].nome}</h2>
           <h3 class="lobo_dir_idade">Idade: ${data[7].idade} anos</h3>
           <p class="lobo_dir_desc">${data[7].descricao}</p>
       </div>
       <img src="${data[7].imagem}" alt="" class="lobo_dir">
   </div>
   
   `

}
function addImagenseDescricaoLobinhos(elementoHTML){
    elementoHTML.innerHTML = criarElementosHTMLIndex(data)
}
