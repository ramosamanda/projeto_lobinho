import data from "./lobinhos.json" assert { type: "json" };

let elementoRecebeDadosLobos = document.querySelector('#lista_lobinhos')
let checkboxAdotados = document.querySelector('#adotados')
let formSubmetido = document.querySelector('form')
let caixaPesquisa = document.querySelector('#procura')


function iterarsobreJson(data){
   return data.map((ele)=> {
        return criarElementosListaLobinho(ele)
    })
}
function criarElementosListaLobinho(data){
    return `<li class="item_lobinhos">
    <img src="${data.imagem}" class="imagem_lobinho">
    <div class="descricao_lobinho">
        <div class="titulo_botao_container">
            <h2 class="titlelobo">${data.nome}</h2>
            <button class="btn" onclick="location.href='show_lobinhos.html'" id="${data.id}">Adotar</button>
        </div>
        <h3 class="subtitulo_idade">${data.idade} anos</h3>
        <p class="descricao_lobinho">${data.descricao}</p>
    </div>
</li>
    `
}
function filtrarLobosAdotados(data){
    return data.map((ele)=> {
        if(ele.adotado){
            return interarSobreJSON(ele)
        }
    })

}
function encontrarLoboPeloNome(nomeProcurado, data){
    return data.map((ele)=> {
        if(ele.nome === nomeProcurado){
            return interarSobreJSON(ele)
        }
    })
}

checkboxAdotados.addEventListener('change',(e)=>{
    e.target.checked ? elementoRecebeDadosLobos.innerHTML = filtrarLobosAdotados(data): elementoRecebeDadosLobos.innerHTML = criarElementosListaLobinho(data)
})

formSubmetido.addEventListener('submit',(e)=>{
    let nomeProcurado = e.target.children[0].value
    if(nomeProcurado){
        elementoRecebeDadosLobos.innerHTML = encontrarLoboPeloNome(nomeProcurado,data)
    }else{
        elementoRecebeDadosLobos.innerHTML = criarElementosListaLobinho(data)
    }
    
})
caixaPesquisa.addEventListener('focusout', ()=>{
    elementoRecebeDadosLobos.innerHTML = iterarsobreJson(data)
})

elementoRecebeDadosLobos.innerHTML = iterarsobreJson(data)


document.querySelectorAll('.btn').forEach((btn)=>{
    btn.addEventListener('click',(e)=>{
        sessionStorage.setItem('id', e.target.id);
    })
})
