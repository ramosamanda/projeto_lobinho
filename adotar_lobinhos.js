import data from "./lobinhos.json" assert { type: "json" };

let cabecalhoComDadosLobinho = document.querySelector('.cabecalho_adocao')
let sessionID = sessionStorage.getItem("id")
let form = document.querySelector('form')
let dadosAdotante = {
    "nome":"",
    "idade":"",
    "email":""
}
cabecalhoComDadosLobinho.innerHTML = data.map((ele)=>{
    if(ele.id == sessionID){
        return criarElementosHTMLIndex(ele)
    }
}).filter((ele)=>{
    if(ele) return ele
})
function criarElementosHTMLIndex(data){
    return `<img src="${data.imagem}" class="imagem_lobinho">
    <h2 class="titlelobo">${data.nome}</h2>
    <h3 class="lobinho_id">${data.id}</h3>
    `
}

function salvarDadosAdodante(target){
    dadosAdotante.nome = target[0].value
    dadosAdotante.idade = target[1].value
    dadosAdotante.email = target[2].value
    console.log(dadosAdotante)
    return dadosAdotante
}

form.addEventListener('submit',(e)=>{
    salvarDadosAdodante(e.target)
})



