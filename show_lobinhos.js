import data from "./lobinhos.json" assert { type: "json" };

let cabecalhoComDadosLobinho = document.querySelector('.content')
let sessionID = sessionStorage.getItem("id")
let dataCopiado = data

console.log(sessionID)
cabecalhoComDadosLobinho.innerHTML = data.map((ele)=>{
    if(ele.id == sessionID){
        return criarElementosHTMLIndex(ele)
    }
}).filter((ele)=>{
    if(ele) return ele
})

function criarElementosHTMLIndex(data){
    let pagina =  `
    <h2 class="titlelobo">${data.nome}</h2>
    <div class="container_imagem_buttons">
        <img src="${data.imagem}" class="imagem_lobinho">
        <button class="btn adotar" onclick="location.href='adotar_lobinhos.html'">Adotar</button>
        <button class="btn excluir" id="excluir">Excluir</button>
    </div>
    <div class="container_descricao">
        <p class="descricao_lobinho">${data.descricao}</p>
    </div>
    
    `
    return pagina
}

if(document.querySelector('#excluir')){
    document.querySelector('#excluir').addEventListener('click',()=>{
    alert('Dado excluido com sucesso')
    dataCopiado.map((ele,index)=>{
        if(ele.id == sessionID){
            console.log(ele)
            delete dataCopiado[index]
        }
    })
})
}

